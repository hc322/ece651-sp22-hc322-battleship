package edu.duke.hc322.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.function.Function;

public class AIPlayer extends TextPlayer{

    public AIPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out, AbstractShipFactory<Character> shipFactory, int moveRemain, int scanRemain) {
        super(name, theBoard, inputSource, out, shipFactory, moveRemain, scanRemain);
    }

    @Override
    public void playOneTurn(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        out.print("Player " + name + "'s turn:\n");
        String result = readOption();
        if(result.equalsIgnoreCase("M")){
            move();
        }else if(result.equalsIgnoreCase("S")){
            scan();
        }else{
            attack(enemyBoard);
        }
    }

    @Override
    public String readOption() throws IOException {
        String s = inputReader.readLine();
        return s;
    }

    @Override
    public void move() throws IOException{
        out.print("Player " + this.name + " used a special action.\n");
        moveRemain--;
    }

    @Override
    public void scan() throws IOException {
        out.print("Player " + this.name + " used a special action.\n");
        scanRemain--;
    }

    @Override
    public void attack(Board<Character> enemyBoard) throws IOException{
        Coordinate c = readCoordinate();
        Ship<Character> aim = enemyBoard.fireAt(c);
        if(aim == null){
            out.print( this.name +" missed!\n");
        }else{
            out.print( this.name + " hit your " + aim.getName() + " at "+ c.toString() +"!\n");
        }
    }

    /** The computer will always read a valid coordinate
     * @return
     * @throws IOException
     */
    public Coordinate readCoordinate() throws IOException {
        String s = inputReader.readLine();
        return new Coordinate(s);
    }

    /** The computer always place the ship at a valid place
     * @param shipName null
     * @param createFn
     * @return always return true
     * @throws IOException
     */
    @Override
    public boolean doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException{
        Placement p = readPlacement();
        Ship<Character> s = createFn.apply(p);
        theBoard.tryAddShip(s);
        out.print(this.view.displayMyOwnBoard());
        return true;
    }


    /** The computer always read a valid placement
     * @return placement
     * @throws IOException
     */
    public Placement readPlacement() throws IOException {
        String s = inputReader.readLine();
        Placement p = new Placement(s);
        return p;
    }

    @Override
    public void placementIntro(){
        out.print("Computer Automatic placement.\n");
    }
}
