package edu.duke.hc322.battleship;

import java.io.*;

public class App {
    TextPlayer player1;
    TextPlayer player2;

    public App(TextPlayer player1, TextPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    /** Ask for twp players to make placement
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException{
        player1.placementIntro();
        for(int i = 0; i < player1.shipsToPlace.size(); i++){
            if (!player1.doOnePlacement(player1.shipsToPlace.get(i), player1.shipCreationFns.get(player1.shipsToPlace.get(i)))){
                i--;
            }
        }
        player2.placementIntro();
        for(int i = 0; i < player2.shipsToPlace.size(); i++){
            if (!player2.doOnePlacement(player2.shipsToPlace.get(i), player2.shipCreationFns.get(player2.shipsToPlace.get(i)))){
                i--;
            }
        }
    }

    /** Ask for players to decide how to play for one turn
     * @throws IOException
     */
    public void doAttackingPhase() throws IOException{
        Board<Character> boardA = player1.theBoard;
        Board<Character> boardB = player2.theBoard;
        while(true){
            if (!boardA.checkLose()){
                player1.playOneTurn(boardB, player2.view, player2.name);
            }else{
                System.out.print("Player B wins!\n");
                break;
            }
            if(!boardB.checkLose()){
                player2.playOneTurn(boardA, player1.view, player1.name);
            }else{
                System.out.print("Player A wins!\n");
                break;
            }
        }
    }

    /** Ask for identity of players: whether human or computer
     * @param prompt is hint that users can see
     * @param input is to read user's input
     * @return
     * @throws IOException
     */
    protected static String askChoice(String prompt, BufferedReader input) throws IOException{
        System.out.print(prompt);
        String s = input.readLine();
        return s;
    }

    /** Create the player based on use's chocie
     * @param name is player's name
     * @param b is the board
     * @param input is the input from the terminal
     * @param fileInput is the input from the file
     * @param factory is the ship factory
     * @return player
     * @throws IOException
     */
    protected static TextPlayer createPlayer(String name, Board<Character> b, BufferedReader input, BufferedReader fileInput, V2ShipFactory factory) throws IOException{
        String result = askChoice("Please specify Player "+ name + ": (any invalid input will be defaulted as 1) \n"
        +"1. Human\n"+ "2. Computer\n", input);
        TextPlayer p = null;
        if(result.equals("2")){
            p = new AIPlayer(name, b, fileInput, System.out, factory, 3, 3);
            return p;
        }
        p = new TextPlayer(name, b, input, System.out, factory, 3, 3);
        return p;

    }

    public static void main(String[] args) throws IOException{
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20,'X');
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String path1 = App.class.getClassLoader().getResource("input1.txt").getPath();
        String path2 = App.class.getClassLoader().getResource("input2.txt").getPath();
        BufferedReader fileInput1 = new BufferedReader(new FileReader(path1));
        BufferedReader fileInput2 = new BufferedReader(new FileReader(path2));
        V2ShipFactory factory = new V2ShipFactory();
        TextPlayer p1 = createPlayer("A", b1, input, fileInput1, factory);
        TextPlayer p2 = createPlayer("B", b2, input, fileInput2, factory);
        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }
}
