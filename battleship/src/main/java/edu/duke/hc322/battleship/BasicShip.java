package edu.duke.hc322.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class BasicShip<T> implements Ship<T>{
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo){
        myPieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c: where) {
            //false, c is part of this ship and has not been hit
            myPieces.put(c, false);
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
    }
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        if(myPieces.containsKey(where)){
            return true;
        }
        return false;
    }

    @Override
    public boolean isSunk() {
        for (Coordinate c : myPieces.keySet()) {
            if (myPieces.get(c) != true){
                return false;
            }
        }
        return true;
    }

    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        checkCoordinateInThisShip(where);
        boolean hitStatus = wasHitAt(where);
        if(myShip){
            return myDisplayInfo.getInfo(where, hitStatus);
        }
        return enemyDisplayInfo.getInfo(where, hitStatus);
    }


    /**Check if the coordinate is part of the ship
     * @param c coordinate
     */
    protected void checkCoordinateInThisShip(Coordinate c){
        if(myPieces.containsKey(c) == false){
            throw new IllegalArgumentException("The coordinate is not part of this ship");
        }
    }

    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }


    @Override
    public String toString(){
        String info = new String();
        Iterable<Coordinate> cs = getCoordinates();
        for(Coordinate c: cs){
            info += c.toString();
            info += ", ";
        }
        info += "\n";
        return info;
    }

    @Override
    public void move(int widthDiff, int heightDiff){
        HashMap<Coordinate, Boolean> new_pieces = new HashMap<>();
        for (Map.Entry<Coordinate, Boolean> entry : myPieces.entrySet()) {
            Coordinate old_c = entry.getKey();
            Boolean old_v = entry.getValue();
            new_pieces.put(new Coordinate(old_c.getRow() + heightDiff, old_c.getColumn() + widthDiff), old_v);
        }
        myPieces = new_pieces;
    }

    @Override
    public void moveBack(int widthDiff, int heightDiff){
        HashMap<Coordinate, Boolean> new_pieces = new HashMap<>();
        for (Map.Entry<Coordinate, Boolean> entry : myPieces.entrySet()) {
            Coordinate old_c = entry.getKey();
            Boolean old_v = entry.getValue();
            new_pieces.put(new Coordinate(old_c.getRow() - heightDiff, old_c.getColumn() - widthDiff), old_v);
        }
        myPieces = new_pieces;
    }

}
