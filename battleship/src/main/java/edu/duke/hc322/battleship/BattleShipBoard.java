package edu.duke.hc322.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;
    private final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    HashMap<Coordinate, T> enemyHits;
    HashSet<Coordinate> enemyMisses;
    final T missInfo;

    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<>(null)), new HashSet<Coordinate>(), missInfo, new HashMap<>());
    }

    /**
     * Constructs a BattleShipBoard with the specified width
     * and height
     * @param width is the width of the newly constructed board.
     * @param height is the height of the newly constructed board.
     * @param placementChecker is a checker
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int width, int height, PlacementRuleChecker<T> placementChecker, HashSet<Coordinate> enemyMisses,
                           T missInfo, HashMap<Coordinate, T> enemyHits){
        if(width <= 0){
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + width);
        }
        if(height <= 0){
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + height);
        }
        this.width = width;
        this.height = height;
        this.myShips = new ArrayList<Ship<T>>();
        this.placementChecker = placementChecker;
        this.enemyMisses = enemyMisses;
        this.missInfo = missInfo;
        this.enemyHits = enemyHits;
    }

    public int getWidth(){
        return this.width;
    }

    public int getHeight(){
        return this.height;
    }

    //public T getMissInfo(){return this.missInfo;}

    public String tryAddShip(Ship<T> toAdd){
        String result = placementChecker.checkPlacement(toAdd, this);
        if (result == null){
            myShips.add(toAdd);
            return null;
        }
        return result;
    }

    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    protected T whatIsAt(Coordinate where, boolean isSelf){
        if(!isSelf){
            if(enemyMisses.contains(where)){
                return missInfo;
            }
            if(enemyHits.containsKey(where)){
                return enemyHits.get(where);
            }
        }
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                return s.getDisplayInfoAt(where, isSelf);
            }
        }
        return null;
    }

    public Ship<T> fireAt(Coordinate c){
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(c) && !s.wasHitAt(c)){
                s.recordHitAt(c);
                enemyHits.put(c, s.getData());
                return s;
            }
        }
        enemyMisses.add(c);
        return null;
    }

    public boolean checkLose(){
        for (Ship ship : myShips){
            if(!ship.isSunk()){
                return false;
            }
        }
        return true;
    }

    public Iterable<Ship<T>> getAllShip(){
        return myShips;
    }

    public void removeShip(Ship<T> ship){
        myShips.remove(ship);
    }


}
