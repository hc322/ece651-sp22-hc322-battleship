package edu.duke.hc322.battleship;

public interface Board<T> {
    public int getWidth();
    public int getHeight();
    public String tryAddShip(Ship<T> toAdd);
    public T whatIsAtForSelf(Coordinate where);
    public T whatIsAtForEnemy(Coordinate where);
    public Ship<T> fireAt(Coordinate c);
    public boolean checkLose();
    public Iterable<Ship<T>> getAllShip();
    public void removeShip(Ship<T> ship);
}
