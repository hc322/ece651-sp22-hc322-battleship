package edu.duke.hc322.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;
    /**
     * Constructs a BoardView, given the board it will display.
     *
     * @param toDisplay is the Board to display
     * @throws IllegalArgumentException if the board is larger than 10x26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn){
        StringBuilder ans = new StringBuilder();
        ans.append(makeHeader());
        char lineAlpha = 'A';
        for(int j = 0; j < toDisplay.getHeight(); j++){
            ans.append(lineAlpha);
            ans.append(" ");
            for (int i = 0; i < toDisplay.getWidth(); i++) {
                if(getSquareFn.apply(new Coordinate(j,i)) != null){
                    ans.append(getSquareFn.apply(new Coordinate(j,i)));
                }else {
                    ans.append(" ");
                }
                if(i != toDisplay.getWidth() - 1){
                    ans.append("|");
                }else{
                    ans.append(" ");
                    ans.append(lineAlpha);
                    ans.append("\n");
                }
            }
            lineAlpha++;
        }
        ans.append(makeHeader());
        return ans.toString();
    }

    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }

    public String displayEnemyBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }

    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        StringBuilder ans = new StringBuilder("     ");
        //Header
        ans.append(myHeader);
        int width = toDisplay.getWidth();
        //(2 * width + 7) spaces between two headers
        for(int i = 0; i < (2 * width + 7); i++){
            ans.append(" ");
        }
        ans.append(enemyHeader);
        ans.append("\n");
        String myView = this.displayMyOwnBoard();
        String otherView = enemyView.displayEnemyBoard();
        String[] myViewLines = myView.split("\n");
        String[] otherViewLines = otherView.split("\n");
        //board
        for(int i = 0; i < myViewLines.length; i++){
            ans.append(myViewLines[i]);
            if(i == 0 || i == myViewLines.length - 1){
                //constant 20 spaces between two boards header
                for(int j = 0; j < 18; j++){
                    ans.append(" ");
                }
            }else{
                //constant 16 spaces between two boards
                for(int j = 0; j < 16; j++){
                    ans.append(" ");
                }
            }
            ans.append(otherViewLines[i]);
            ans.append("\n");
        }
        return ans.toString();
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board
     */
    public String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }


}