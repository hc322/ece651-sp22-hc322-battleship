package edu.duke.hc322.battleship;

import java.util.Objects;

public class Coordinate {
    private int row;
    private int column;

    public Coordinate(int r, int c){
//        if(r < 0 || c < 0){
            //throw new IllegalArgumentException("The parameter of coordinate is invalid!");
        //}else{
            row = r;
            column = c;
        //}
    }

    public Coordinate(String descr){
        if (descr.length() != 2){
            throw new IllegalArgumentException("The length of the input is limited to 2 but is " + descr.length());
        }
        String upperDescr = descr.toUpperCase();
        char firstLetter = upperDescr.charAt(0);
        if (firstLetter < 'A' || firstLetter > 'Z'){
            throw new IllegalArgumentException("The first letter should be in range of A-Z (case ignored) but is "+ firstLetter);
        }
        char secondNum = descr.charAt(1);
        if (secondNum > '9' || secondNum < '0'){
            throw new IllegalArgumentException("The second number should be in range of 0-9 but is " + secondNum);
        }
        row = firstLetter - 'A';
        column = secondNum - '0';
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return row == that.row && column == that.column;
    }

    @Override
    public String toString() {
        String row = String.valueOf((char)(this.row + 'A'));
        String column = String.valueOf(this.column);
        return row+column;
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
