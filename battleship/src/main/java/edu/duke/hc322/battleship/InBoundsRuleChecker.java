package edu.duke.hc322.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        Iterable<Coordinate> set = theShip.getCoordinates();
        for(Coordinate c : set){
            int column = c.getColumn();
            int row = c.getRow();
            if(column >= theBoard.getWidth() ){
                return "That placement is invalid: the ship goes off the right of the board.\n";
            }else if (column < 0){
                return "That placement is invalid: the ship goes off the left of the board.\n";
            }else if(row >= theBoard.getHeight() ){
                return "That placement is invalid: the ship goes off the bottom of the board.\n";
            }else if(row < 0){
                return "That placement is invalid: the ship goes off the top of the board.\n";
            }
        }
        return null;
    }

}
