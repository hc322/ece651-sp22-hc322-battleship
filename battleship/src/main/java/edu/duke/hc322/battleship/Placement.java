package edu.duke.hc322.battleship;

import java.util.Objects;

public class Placement {
    private final Coordinate coordinate;
    private final char orientation;


    /**Constructor
     * @param c coordinate
     * @param o orientation (case ignored)
     */
    public Placement(Coordinate c, char o) {
        String temp = String.valueOf(o).toUpperCase();
        orientation = temp.charAt(0);
        coordinate = c;
    }

    public Placement(String descr){
        if(descr.length() != 3){
            throw new IllegalArgumentException("The length of the input should be 3 but is "+ descr.length());
        }
        String upperDescr = descr.toUpperCase();
        coordinate = new Coordinate(upperDescr.substring(0,2));
        char temp = upperDescr.charAt(2);
        if(temp != 'V' && temp != 'H' && temp != 'U' && temp != 'R' &&temp != 'D' && temp != 'L'){
            throw new IllegalArgumentException("The third letter should be 'V','H','U','R','D','L' but is "+ temp);
        }else{
            orientation = temp;
        }
    }

    public char getOrientation() {
        return orientation;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    @Override
    public String toString() {
        return "Placement{" +
                "coordinate=" + coordinate +
                ", orientation=" + orientation +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Placement placement = (Placement) o;
        return orientation == placement.orientation && coordinate.equals(placement.coordinate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinate, orientation);
    }


}
