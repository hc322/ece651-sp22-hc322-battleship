package edu.duke.hc322.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T>{
    private final String name;
    private final T data;
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, data, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, null));
    }
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, ShipDisplayInfo<T> info, ShipDisplayInfo<T> enemyInfo) {
        super(makeCoords(upperLeft, width, height), info, enemyInfo);
        this.name = name;
        this.data = data;
    }

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height){
        HashSet<Coordinate> hashSet = new HashSet<>();
        for(int i = upperLeft.getColumn(); i < width + upperLeft.getColumn(); i++){
            for(int j = upperLeft.getRow(); j < height + upperLeft.getRow(); j++){
                hashSet.add(new Coordinate(j, i));
            }
        }
        return hashSet;
    }

    public String getName(){
        return name;
    }
    public T getData(){return data;}

    @Override
    public String toString(){
        String info = new String();
        Iterable<Coordinate> cs = getCoordinates();
        info += name;
        info += " contains: ";
        for(Coordinate c: cs){
            info += c.toString();
            info += ", ";
        }
        info += "\n";
        return info;
    }

}
