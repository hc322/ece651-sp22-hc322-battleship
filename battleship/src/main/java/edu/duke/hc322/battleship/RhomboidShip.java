package edu.duke.hc322.battleship;

import java.util.HashSet;

public class RhomboidShip<T> extends BasicShip<T>{
    private final String name;
    private final Character orientation;
    private final T data;
    public RhomboidShip(String name, Coordinate upperLeft, int width, int height, Character orientation, T data, T onHit) {
        this(name, upperLeft, width, height, data, orientation, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, null));
    }

    public RhomboidShip(String name, Coordinate upperLeft, int width, int height, T data, Character orientation, ShipDisplayInfo<T> info, ShipDisplayInfo<T> enemyInfo) {
        super(makeCoords(upperLeft, width, height, orientation), info, enemyInfo);
        this.name = name;
        this.orientation = orientation;
        this.data = data;
    }

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height, Character orientation){
        HashSet<Coordinate> hashSet = new HashSet<>();
        for(int i = upperLeft.getColumn(); i < width + upperLeft.getColumn(); i++){
            for(int j = upperLeft.getRow(); j < height + upperLeft.getRow(); j++){
                hashSet.add(new Coordinate(j, i));
            }
        }
        if(orientation == 'U'){
            hashSet.remove(new Coordinate(upperLeft.getRow() + 4,upperLeft.getColumn()));
            hashSet.remove(new Coordinate(upperLeft.getRow(),upperLeft.getColumn() + 1));
            hashSet.remove(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn() + 1));
        }else if(orientation == 'R'){
            hashSet.remove(upperLeft);
            hashSet.remove(new Coordinate(upperLeft.getRow() +1, upperLeft.getColumn()+3));
            hashSet.remove(new Coordinate(upperLeft.getRow() +1, upperLeft.getColumn()+4));
        }else if(orientation == 'D'){
            hashSet.remove(new Coordinate(upperLeft.getRow(), upperLeft.getColumn()+1));
            hashSet.remove(new Coordinate(upperLeft.getRow()+3, upperLeft.getColumn()));
            hashSet.remove(new Coordinate(upperLeft.getRow()+4, upperLeft.getColumn()));
        }else if(orientation == 'L'){
            hashSet.remove(upperLeft);
            hashSet.remove(new Coordinate(upperLeft.getRow(), upperLeft.getColumn()+1));
            hashSet.remove(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()+4));
        }
        return hashSet;
    }

    public String getName(){
        return name;
    }

    public T getData(){return data;}
    @Override
    public String toString(){
        String info = new String();
        Iterable<Coordinate> cs = getCoordinates();
        info += name;
        info += " contains: ";
        for(Coordinate c: cs){
            info += c.toString();
            info += ", ";
        }
        info += "\n";
        return info;
    }
}
