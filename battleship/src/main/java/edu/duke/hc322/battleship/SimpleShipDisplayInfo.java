package edu.duke.hc322.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    T myData;
    T onHit;

    public SimpleShipDisplayInfo(T data, T hit){
        this.myData  = data;
        this.onHit = hit;
    }

    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if(hit){
            return onHit;
        }else{
            return myData;
        }
    }
}
