package edu.duke.hc322.battleship;

import java.io.*;
import java.util.*;
import java.util.function.Function;

public class TextPlayer {
    final String name;
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    int moveRemain;
    int scanRemain;

    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out, AbstractShipFactory<Character> shipFactory, int moveRemain, int scanRemain) {
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputSource;
        this.out = out;
        this.shipFactory = shipFactory;
        this.shipsToPlace = new ArrayList<String>();
        setupShipCreationList();
        this.shipCreationFns = new HashMap<>();
        setupShipCreationMap();
        this.moveRemain = moveRemain;
        this.scanRemain = scanRemain;
    }

    protected void setupShipCreationMap(){
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    }

    protected void setupShipCreationList(){
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    /** Read the placement from user's input
     * @param prompt is some hints
     * @return the placement
     * @throws IOException
     */
    public Placement readPlacement(String prompt) throws IOException {
        out.print(prompt);
        String s = inputReader.readLine();
        if(s == null){
            throw new EOFException();
        }
        Placement p = null;
        try{
            p = new Placement(s);
        }catch (IllegalArgumentException e){
            return null;
        }
        return p;
    }

    public void placementIntro(){
        out.print(view.displayMyOwnBoard());
//        String instruction = "Player " + name + ": you are going to place the following ships (which are all\n" +
//                "rectangular). For each ship, type the coordinate of the upper left\n" +
//                "side of the ship, followed by either H (for horizontal) or V (for\n" +
//                "vertical).  For example M4H would place a ship horizontally starting\n" +
//                "at M4 and going to the right.  You have\n" +
//                "\n" +
//                "2 \"Submarines\" ships that are 1x2 \n" +
//                "3 \"Destroyers\" that are 1x3\n" +
//                "3 \"Battleships\" that are 1x4\n" +
//                "2 \"Carriers\" that are 1x6\n";
//        out.print(instruction);
    }

    /** Place one ship
     * @param shipName is ship name
     * @param createFn is to choose which ship to place
     * @return
     * @throws IOException
     */
    public boolean doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException{
        Placement p = readPlacement("Player " + name + " Where do you want to place a " + shipName + "?\n");
        while(p == null || ((shipName.equals("Submarine") || shipName.equals("Destroyer")) && (p.getOrientation() != 'H' &&
                p.getOrientation() != 'V')) || ((shipName.equals("Battleship") || shipName.equals("Carrier")) && (p.getOrientation() != 'U' &&
                p.getOrientation() != 'R' && p.getOrientation() != 'D' && p.getOrientation() != 'L'))){
            if(p == null){
                out.print("That placement is invalid: it does not have the correct format.\n");
            }else{
                out.print("That placement is invalid: The orientation is invalid.\n");
            }
            p = readPlacement("Player " + name + " Where do you want to place a " + shipName + "?\n");
        }
        Ship<Character> s = createFn.apply(p);
        String message = theBoard.tryAddShip(s);
        if(message == null){
            out.print(view.displayMyOwnBoard());
            return true;
        }else{
            out.print(message);
            return false;
        }

    }

    /** Read the coordinate from the user
     * @param prompt is some hints
     * @return the coordinate
     * @throws IOException
     */
    public Coordinate readCoordinate(String prompt) throws IOException {
        out.print(prompt);
        String s = inputReader.readLine();
        if(s == null){
            throw new EOFException();
        }
        try{
            Coordinate c = new Coordinate(s);
        }catch (IllegalArgumentException e){
            out.print("That coordinate is invalid: it does not have the correct format.\n");
            return null;
        }
        return new Coordinate(s);
    }

    /** read user's input for what to do in a turn
     * @return the input
     * @throws IOException
     */
    public String readOption() throws IOException {
        out.print("Possible actions for Player " + name + ":\n" +
                "\n" +
                " F Fire at a square\n" +
                " M Move a ship to another square (" + moveRemain + " remaining)\n" +
                " S Sonar scan (" + scanRemain + " remaining)\n" +
                "\n" +
                "Player " + name +", what would you like to do?\n");
        String s = inputReader.readLine();
        if(s == null){
            throw new EOFException();
        }
        if((s.equalsIgnoreCase("M") && moveRemain != 0) || (s.equalsIgnoreCase("S") && scanRemain != 0)){
            return s;
        }else if (s.equalsIgnoreCase("F")){
            return s;
        } else{
            out.print("The input action is not valid or available.\n");
            return null;
        }
    }

    /** Attack on enemy;s board
     * @param enemyBoard is the board for enemy
     * @throws IOException
     */
    public void attack(Board<Character> enemyBoard) throws IOException{
        Coordinate c = readCoordinate("Player " + name + ", Enter the coordinate to fire at:\n");
        while(c == null){
            c = readCoordinate("Player " + name + ", Enter the coordinate to fire at:\n");
        }
        Ship<Character> aim = enemyBoard.fireAt(c);
        if(aim == null){
            out.print("You missed!\n");
        }else{
            out.print("You hit a " + aim.getName() + "!\n");
        }
    }

    /** Keep asking coordinate if it is not in the board
     * @return the coordinate in the board
     * @throws IOException
     */
    public Coordinate askCoordinateInBoard(String prompt) throws IOException{
        Coordinate c_dst = readCoordinate(prompt);
        while(c_dst == null || c_dst.getRow() > 19){
            if(c_dst.getRow() > 19){
                out.print("The coordinate is out of the board.\n");
            }
            c_dst = readCoordinate(prompt);
        }
        return c_dst;
    }

    /** Keep asking the coordinate of a ship that users want to move
     * @param map the coordinate should be the key of the map
     * @return  expected coordinate
     * @throws IOException
     */
    public Coordinate askIncludedCoordinate(HashMap<Coordinate, Ship<Character>> map) throws IOException{
        Coordinate c_src = readCoordinate("Player " + name + ", enter an included coordinate:\n");
        while(c_src == null || !map.containsKey(c_src)){
            if(!map.containsKey(c_src)){
                out.print("The coordinate is not included.\n");
            }
            c_src = readCoordinate("Player " + name + ", enter an included coordinate:\n");
        }
        return c_src;
    }

    /** Create a map whose key is coordinate and value is ship
     * @return map
     */
    public HashMap<Coordinate, Ship<Character>> createMap(){
        HashMap<Coordinate, Ship<Character>> map = new HashMap<>();
        Iterable<Ship<Character>> ships = theBoard.getAllShip();
        int index = 0;
        for(Ship<Character> ship : ships){
            Iterable<Coordinate> cs = ship.getCoordinates();
            for(Coordinate c : cs){
                map.put(c, ship);
            }
            out.print(++index + ". ");
            out.print(ship.toString());
        }
        return map;
    }

    /** Try to move (not really move)
     * @param map whose key is coordinate and value is ship
     * @param c_src is the source of movement
     * @param c_dst is the destination of movement
     * @return false if out of bounds or overlap
     *          true if success
     */
    public boolean tryMove(HashMap<Coordinate, Ship<Character>> map, Coordinate c_src, Coordinate c_dst){
        Ship<Character> targetShip = map.get(c_src);
        theBoard.removeShip(targetShip);
        int widthDiff = c_dst.getColumn() - c_src.getColumn();
        int heightDiff = c_dst.getRow() - c_src.getRow();
        targetShip.move(widthDiff,heightDiff);
        String moveResult = theBoard.tryAddShip(targetShip);
        if(moveResult != null){
            out.print(moveResult);
            targetShip.moveBack(widthDiff,heightDiff);
            theBoard.tryAddShip(targetShip);
            return false;
        }
        return true;
    }

    /** Keep moving if tryMove fails
     * @throws IOException
     */
    public void move() throws IOException{
        HashMap<Coordinate, Ship<Character>> map = createMap();
        Coordinate c_src = askIncludedCoordinate(map);
        Coordinate c_dst = askCoordinateInBoard("Player " + name + ", enter the coordinate you want to move to:\n");
        boolean moveSuccess = tryMove(map, c_src, c_dst);
        while(!moveSuccess){
            c_dst = askCoordinateInBoard("Player " + name + ", enter the coordinate you want to move to:\n");
            moveSuccess = tryMove(map, c_src, c_dst);
        }
        moveRemain--;
    }

    /** Sonar scan
     * @throws IOException
     */
    public void scan() throws IOException{
        Coordinate center = askCoordinateInBoard("Player " + name + ", enter the central coordinate you want to scan:\n");
        HashMap<Character, Integer> count = new HashMap<>();
        count.put('s', 0);
        count.put('b', 0);
        count.put('c', 0);
        count.put('d', 0);

        traverse(count, 3, new Coordinate(center.getRow(), center.getColumn()));
        traverse(count, 2, new Coordinate(center.getRow()-1, center.getColumn()));
        traverse(count, 2, new Coordinate(center.getRow()+1, center.getColumn()));
        traverse(count, 1, new Coordinate(center.getRow()-2, center.getColumn()));
        traverse(count, 1, new Coordinate(center.getRow()+2, center.getColumn()));
        traverse(count, 0, new Coordinate(center.getRow()-3, center.getColumn()));
        traverse(count, 0, new Coordinate(center.getRow()+3, center.getColumn()));

        out.print("Submarines occupy " + count.get('s') + " squares\n" +
                "Destroyers occupy " + count.get('d') + " squares\n" +
                "Battleships occupy " + count.get('b') + " squares\n" +
                "Carriers occupy " + count.get('c') +" square\n");
        scanRemain--;
    }

    public void playOneTurn(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException{
        out.print("Player " + name + "'s turn:\n");
        out.print(this.view.displayMyBoardWithEnemyNextToIt(enemyView, "Your ocean", "Player " + enemyName + "'s ocean"));
        String result = readOption();
        while(result == null){
            result = readOption();
        }
        if(result.equalsIgnoreCase("F")){
            attack(enemyBoard);
        }else if(result.equalsIgnoreCase("M")){
            move();
        }else if(result.equalsIgnoreCase("S")){
            scan();
        }
    }

    /** Increase the frequency by 1
     * @param count is the map
     * @param toAdd is the key
     */
    public void updateCount(HashMap<Character, Integer> count, Character toAdd){
        if(toAdd == null) return;
        int oldValue = count.get(toAdd);
        oldValue++;
        count.put(toAdd, oldValue);
    }

    public void traverse(HashMap<Character, Integer> count, int offset, Coordinate center){
        //center
        Character ans = theBoard.whatIsAtForSelf(new Coordinate(center.getRow(), center.getColumn()));
        updateCount(count, ans);
        //right
        for(int i = 1; i <= offset; i++){
            ans = theBoard.whatIsAtForSelf(new Coordinate(center.getRow(), center.getColumn() + i));
            updateCount(count, ans);
        }
        //left
        for(int i = 1; i <= offset; i++){
            ans = theBoard.whatIsAtForSelf(new Coordinate(center.getRow(), center.getColumn() - i));
            updateCount(count, ans);
        }
    }
}
