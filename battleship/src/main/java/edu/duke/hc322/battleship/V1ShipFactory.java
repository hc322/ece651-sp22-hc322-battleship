package edu.duke.hc322.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character>{

    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
        Ship<Character> ship;
        if(where.getOrientation() == 'V'){
            ship = new RectangleShip<Character>(name, where.getCoordinate(), w, h, letter, '*');
        }else{
            ship = new RectangleShip<Character>(name, where.getCoordinate(), h, w, letter, '*');
        }
        return ship;
    }
    //Submarine:   1x2 's'
    //Destroyer:   1x3 'd'
    //Battleship:  1x4 'b'
    //Carrier:     1x6 'c'

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "BattleShip");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }
}
