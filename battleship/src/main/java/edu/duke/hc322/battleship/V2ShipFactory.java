package edu.duke.hc322.battleship;

public class V2ShipFactory implements AbstractShipFactory<Character>{
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
        Ship<Character> ship = null;
        if(name.equals("Submarine") || name.equals("Destroyer")){
            if(where.getOrientation() == 'V'){
                ship = new RectangleShip<Character>(name, where.getCoordinate(), w, h, letter, '*');

            }else if(where.getOrientation() == 'H'){
                ship = new RectangleShip<Character>(name, where.getCoordinate(), h, w, letter, '*');
            }
        }else if(name.equals("Battleship")){
            if(where.getOrientation() == 'U' || where.getOrientation() == 'D'){
                ship = new TriangleShip<>(name, where.getCoordinate(), h, w, where.getOrientation(), letter, '*');
            }else if(where.getOrientation() == 'R' || where.getOrientation() == 'L'){
                ship = new TriangleShip<>(name, where.getCoordinate(), w, h, where.getOrientation(), letter, '*');
            }
        }else if(name.equals("Carrier")){
            if(where.getOrientation() == 'U' || where.getOrientation() == 'D'){
                ship = new RhomboidShip<>(name, where.getCoordinate(), w, h, where.getOrientation(), letter, '*');
            }else if(where.getOrientation() == 'R' || where.getOrientation() == 'L'){
                ship = new RhomboidShip<>(name, where.getCoordinate(), h, w, where.getOrientation(), letter, '*');
            }
        }
        return ship;
    }
    //Submarine:   1x2 's'
    //Destroyer:   1x3 'd'

    //Battleship:   *b      OR    B         Bbb        *b
    //              bbb           bb   OR    b     OR  bb
    //                            b                     b
    //
    //               Up          Right      Down      Left

    //Carrier:          C                       C
    //                  c          *cccc        cc       * ccc
    //                  cc   OR    ccc      OR  cc   OR  cccc
    //                  cc                       c
    //                   c                       c
    //
    //                 Up           Right     Down          Left

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 2, 3, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 2, 5, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }


}
