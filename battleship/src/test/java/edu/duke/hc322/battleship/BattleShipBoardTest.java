package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }
    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, -5,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(-8, 20,'X'));
    }

    @Test
    public void test_what_is_at(){
        BattleShipBoard<Character> b = new BattleShipBoard<>(2, 2,'X');
        Coordinate c1 = new Coordinate(0,0);
        Coordinate c2 = new Coordinate(0,1);
        Coordinate c3 = new Coordinate(1,0);
        Coordinate c4 = new Coordinate(1,1);
        //empty board
        assertEquals(null, b.whatIsAtForSelf(c1));
        assertEquals(null, b.whatIsAtForSelf(c2));
        assertEquals(null, b.whatIsAtForSelf(c3));
        assertEquals(null, b.whatIsAtForSelf(c4));
        //put one ship
        BasicShip s1 = new RectangleShip<Character>(c1, 's', '*');
        assertNull(b.tryAddShip(s1));
        assertEquals('s', b.whatIsAtForSelf(c1));


    }

    @Test
    void test_try_add(){
        BattleShipBoard<Character> b = new BattleShipBoard<>(5, 5,'X');
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement v6_6 = new Placement(new Coordinate(6, 6), 'V');
        Ship<Character> ship_1 = f.makeDestroyer(v1_2);
        Ship<Character> ship_2 = f.makeDestroyer(v6_6);
        assertNull(b.tryAddShip(ship_1));
        assertEquals(b.tryAddShip(ship_1), "That placement is invalid: the ship overlaps another ship.\n");
        assertEquals(b.tryAddShip(ship_2), "That placement is invalid: the ship goes off the right of the board.\n");
    }

    @Test
    void test_fireAt(){
        BattleShipBoard<Character> b = new BattleShipBoard<>(5, 5,'X');
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> ship_1 = f.makeDestroyer(v1_2);
        b.tryAddShip(ship_1);
        assertSame(b.fireAt(new Coordinate(1,2)), ship_1);
        b.fireAt(new Coordinate(1,2));
        assertFalse(ship_1.isSunk());
        b.fireAt(new Coordinate(2,2));
        assertFalse(ship_1.isSunk());
        b.fireAt(new Coordinate(3,2));
        assertTrue(ship_1.isSunk());
        assertNull(b.fireAt(new Coordinate(1,3)));
    }

    @Test
    void test_what_is_at_enemy(){
        BattleShipBoard<Character> b = new BattleShipBoard<>(5, 5,'X');
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(0, 0), 'V');
        Ship<Character> ship_1 = f.makeDestroyer(v1_2);
        b.tryAddShip(ship_1);
        assertEquals(b.whatIsAtForSelf(new Coordinate(1,0)), 'd');
        assertEquals(b.whatIsAtForSelf(new Coordinate(2,0)), 'd');
        b.fireAt(new Coordinate(3,0));
        assertEquals(b.whatIsAtForEnemy(new Coordinate(3,0)), 'X');
        //assertNull(b.fireAt(new Coordinate(1,3)));
    }

    @Test
    void test_check_lose(){
        Board<Character> b1 = new BattleShipBoard(4, 3,'X');
        RectangleShip<Character> s1 = new RectangleShip<Character>(new Coordinate(1,0), 's', '*');
        b1.tryAddShip(s1);
        assertFalse(b1.checkLose());
        b1.fireAt(new Coordinate(1,0));
        assertTrue(b1.checkLose());
    }
}