package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardTextViewTest {

    @Test
    void test_display_empty_2by2() {
        /*Board b1 = new BattleShipBoard(2, 2);
        BoardTextView view = new BoardTextView(b1);
        String expected=
                        "  0|1\n"+
                        "A  |  A\n"+
                        "B  |  B\n"+
                        "  0|1\n";
        assertEquals(expected, view.displayMyOwnBoard());*/
        Board<Character> b1 = new BattleShipBoard(2, 2,'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader= "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected=
                expectedHeader+
                        "A  |  A\n"+
                        "B  |  B\n"+
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard(11,20,'X');
        Board<Character> tallBoard = new BattleShipBoard(10,27,'X');
        //you should write two assertThrows here
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }
    @Test
    public void test_display_empty_3by2(){
        emptyBoardHelper(3, 2, "  0|1|2\n",
                "A  | |  A\n"+
                        "B  | |  B\n");
    }

    @Test
    public void test_display_empty_3by5(){
        emptyBoardHelper(3, 5, "  0|1|2\n",
                "A  | |  A\n"+
                        "B  | |  B\n"+
                        "C  | |  C\n"+
                        "D  | |  D\n"+
                        "E  | |  E\n");
    }

    @Test
    public void test_display_empty_4by3(){
        emptyBoardHelper(4, 3, "  0|1|2|3\n",
                "A  | | |  A\n"+
                        "B  | | |  B\n"+
                        "C  | | |  C\n");
    }

    @Test
    public void test_display_nonempty_4by3(){
        Board<Character> b1 = new BattleShipBoard(4, 3,'X');
        RectangleShip<Character> s1 = new RectangleShip<Character>(new Coordinate(1,1), 's', '*');
        RectangleShip<Character> s2 = new RectangleShip<Character>(new Coordinate(1,2), 's', '*');
        b1.tryAddShip(s1);
        b1.tryAddShip(s2);
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader = "  0|1|2|3\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expectedBody = "A  | | |  A\n"+
                              "B  |s|s|  B\n"+
                              "C  | | |  C\n";
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    void test_display_enemy_board(){
        Board<Character> b1 = new BattleShipBoard(4, 3,'X');
        RectangleShip<Character> s1 = new RectangleShip<Character>(new Coordinate(1,0), 's', '*');
        RectangleShip<Character> s2 = new RectangleShip<Character>(new Coordinate(1,1), 's', '*');
        RectangleShip<Character> s3 = new RectangleShip<Character>(new Coordinate(0,3), 'd', '*');
        RectangleShip<Character> s4 = new RectangleShip<Character>(new Coordinate(1,3), 'd', '*');
        RectangleShip<Character> s5 = new RectangleShip<Character>(new Coordinate(2,3), 'd', '*');
        b1.tryAddShip(s1);
        b1.tryAddShip(s2);
        b1.tryAddShip(s3);
        b1.tryAddShip(s4);
        b1.tryAddShip(s5);
        BoardTextView view = new BoardTextView(b1);
        String myView =
                "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B s|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";
        String enemyView =
                "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B  | | |  B\n" +
                        "C  | | |  C\n" +
                        "  0|1|2|3\n";
        //make sure we laid things out the way we think we did.
        assertEquals(myView, view.displayMyOwnBoard());
        assertEquals(enemyView, view.displayEnemyBoard());
        b1.fireAt(new Coordinate(1,0));
        //b1.fireAt(new Coordinate(1,1));
        b1.fireAt(new Coordinate(2,0));
        assertEquals(b1.whatIsAtForEnemy(new Coordinate(1,0)), 's');
        assertEquals(b1.whatIsAtForSelf(new Coordinate(1,0)), '*');
        assertEquals(b1.whatIsAtForEnemy(new Coordinate(2,0)), 'X');
        String enemyView_after =
                "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B s| | |  B\n" +
                        "C X| | |  C\n" +
                        "  0|1|2|3\n";
        assertEquals(view.displayEnemyBoard(), enemyView_after);
    }



    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard(w, h,'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }


    @Test
    void test_display_next(){
        Board<Character> b1 = new BattleShipBoard(4, 3,'X');
        RectangleShip<Character> s1 = new RectangleShip<Character>(new Coordinate(1,0), 's', '*');
        RectangleShip<Character> s2 = new RectangleShip<Character>(new Coordinate(1,1), 's', '*');
        RectangleShip<Character> s3 = new RectangleShip<Character>(new Coordinate(0,3), 'd', '*');
        RectangleShip<Character> s4 = new RectangleShip<Character>(new Coordinate(1,3), 'd', '*');
        RectangleShip<Character> s5 = new RectangleShip<Character>(new Coordinate(2,3), 'd', '*');
        b1.tryAddShip(s1);
        b1.tryAddShip(s2);
        b1.tryAddShip(s3);
        b1.tryAddShip(s4);
        b1.tryAddShip(s5);
        Board<Character> b2 = new BattleShipBoard(4, 3,'X');
        RectangleShip<Character> s6 = new RectangleShip<Character>(new Coordinate(1,0), 's', '*');
        RectangleShip<Character> s7 = new RectangleShip<Character>(new Coordinate(1,1), 's', '*');
        RectangleShip<Character> s8 = new RectangleShip<Character>(new Coordinate(0,3), 'd', '*');
        RectangleShip<Character> s9 = new RectangleShip<Character>(new Coordinate(1,3), 'd', '*');
        RectangleShip<Character> s10 = new RectangleShip<Character>(new Coordinate(2,3), 'd', '*');
        b2.tryAddShip(s6);
        b2.tryAddShip(s7);
        b2.tryAddShip(s8);
        b2.tryAddShip(s9);
        b2.tryAddShip(s10);
        BoardTextView view = new BoardTextView(b1);
        BoardTextView view_1 = new BoardTextView(b2);
        String myView =
                "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B s|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";
        b1.fireAt(new Coordinate(1,0));
        b1.fireAt(new Coordinate(2,0));
        b1.fireAt(new Coordinate(0,3));
        b2.fireAt(new Coordinate(1,0));
        b2.fireAt(new Coordinate(2,0));
        b2.fireAt(new Coordinate(2,1));
        //assertNull(view.displayMyBoardWithEnemyNextToIt(view_1, "Your ocean", "Player B's ocean"));
        assertEquals( "     Your ocean               Player B's ocean\n" +
                "  0|1|2|3                    0|1|2|3\n" +
                "A  | | |d A                A  | | |d A\n" +
                "B *|s| |d B                B s| | |  B\n" +
                "C  | | |d C                C X| | |  C\n" +
                "  0|1|2|3                    0|1|2|3\n", view_1.displayMyBoardWithEnemyNextToIt(view, "Your ocean", "Player B's ocean"));
    }


}