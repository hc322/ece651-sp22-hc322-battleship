package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class InBoundsRuleCheckerTest {

    @Test
    void test_checkMyRule() {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst1_2 = f.makeDestroyer(v1_2);
        Placement vm1_2 = new Placement(new Coordinate(-1, 2), 'V');
        Ship<Character> dstm1_2 = f.makeDestroyer(vm1_2);
        Placement v1_m2 = new Placement(new Coordinate(1, -2), 'V');
        Ship<Character> dst1_m2 = f.makeDestroyer(v1_m2);
        Placement v1_6 = new Placement(new Coordinate(1, 6), 'V');
        Ship<Character> dst1_6 = f.makeDestroyer(v1_6);
        Placement v6_1 = new Placement(new Coordinate(6, 1), 'V');
        Ship<Character> dst6_1 = f.makeDestroyer(v6_1);
        PlacementRuleChecker<Character> checker = new InBoundsRuleChecker<>(null);
        BattleShipBoard<Character> bsb = new BattleShipBoard<>(5,5, checker, new HashSet<>(),'X', new HashMap<>());
        assertEquals(checker.checkMyRule(dstm1_2,bsb), "That placement is invalid: the ship goes off the top of the board.\n");
        assertEquals(checker.checkMyRule(dst1_m2,bsb), "That placement is invalid: the ship goes off the left of the board.\n");
        assertEquals(checker.checkMyRule(dst1_6,bsb), "That placement is invalid: the ship goes off the right of the board.\n");
        assertEquals(checker.checkMyRule(dst6_1,bsb), "That placement is invalid: the ship goes off the bottom of the board.\n");
        assertNull(checker.checkMyRule(dst1_2,bsb));

    }
}