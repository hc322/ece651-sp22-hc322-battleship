package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class NoCollisionRuleCheckerTest {

    @Test
    void test_check_collision() {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement v2_1 = new Placement(new Coordinate(2, 1), 'V');
        Ship<Character> dst = f.makeDestroyer(v1_2);
        PlacementRuleChecker<Character> checker = new NoCollisionRuleChecker<>(null);
        BattleShipBoard<Character> bsb = new BattleShipBoard<>(5,5, checker, new HashSet<>(),'X', new HashMap<>());
        bsb.tryAddShip(dst);
        Ship<Character> dst_copy = f.makeDestroyer(v1_2);
        Ship<Character> dst_1 = f.makeDestroyer(v2_1);
        assertEquals(checker.checkMyRule(dst_copy,bsb), "That placement is invalid: the ship overlaps another ship.\n");
        assertEquals(checker.checkMyRule(dst,bsb), "That placement is invalid: the ship overlaps another ship.\n");
        assertNull(checker.checkMyRule(dst_1,bsb));
    }

    @Test
    void test_combined_collsion_bounds(){
        PlacementRuleChecker<Character> checker = new InBoundsRuleChecker<>(new NoCollisionRuleChecker<>(null));
        BattleShipBoard<Character> bsb = new BattleShipBoard<Character>(5,5, checker, new HashSet<>(),'X', new HashMap<>());
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst1_2 = f.makeDestroyer(v1_2);
        bsb.tryAddShip(dst1_2);
        Placement vm1_2 = new Placement(new Coordinate(-1, 2), 'V');
        Ship<Character> dstm1_2 = f.makeDestroyer(vm1_2);
        Placement v1_m2 = new Placement(new Coordinate(1, -2), 'V');
        Ship<Character> dst1_m2 = f.makeDestroyer(v1_m2);
        Placement v1_6 = new Placement(new Coordinate(1, 6), 'V');
        Ship<Character> dst1_6 = f.makeDestroyer(v1_6);
        Placement v6_1 = new Placement(new Coordinate(6, 1), 'V');
        Ship<Character> dst6_1 = f.makeDestroyer(v6_1);

        assertEquals(checker.checkPlacement(dstm1_2,bsb), "That placement is invalid: the ship goes off the top of the board.\n");
        assertEquals(checker.checkPlacement(dst1_m2,bsb), "That placement is invalid: the ship goes off the left of the board.\n");
        assertEquals(checker.checkPlacement(dst1_6,bsb), "That placement is invalid: the ship goes off the right of the board.\n");
        assertEquals(checker.checkPlacement(dst6_1,bsb), "That placement is invalid: the ship goes off the bottom of the board.\n");
        assertEquals(checker.checkPlacement(dst1_2,bsb), "That placement is invalid: the ship overlaps another ship.\n");


    }
}