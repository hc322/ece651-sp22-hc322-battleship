package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlacementTest {
    @Test
    void test_get_c_o() {
        Coordinate c = new Coordinate("A0");
        Placement p = new Placement(c, 'H');
        Placement p1 = new Placement("A0V");
        assertEquals(c, p.getCoordinate());
        assertEquals('H', p.getOrientation());
        assertEquals(c, p1.getCoordinate());
        assertEquals('V', p1.getOrientation());
    }


    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(1, 3);
        Coordinate c4 = new Coordinate(3, 2);
        Placement p1 = new Placement(c1, 'h');
        Placement p2 = new Placement(c2, 'H');
        Placement p3 = new Placement(c3, 'H');
        Placement p4 = new Placement(c4, 'H');
        assertEquals(p1, p1);   //equals should be reflexsive
        assertEquals(p1, p2);   //different objects but same contents
        assertNotEquals(p1, p3);  //different contents
        assertNotEquals(p1, p4);
        assertNotEquals(p3, p4);
        assertNotEquals(p1, "B2H"); //different types
    }

    @Test
    public void test_hashCode() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(0, 3);
        Coordinate c4 = new Coordinate(2, 1);
        Placement p1 = new Placement(c1, 'H');
        Placement p2 = new Placement(c2, 'H');
        Placement p3 = new Placement(c3, 'H');
        Placement p4 = new Placement(c4, 'H');
        assertEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p1.hashCode(), p4.hashCode());
    }

    @Test
    void test_string_constructor_valid_cases() {
        Coordinate c1 = new Coordinate("b3");
        Placement p1 = new Placement("b3h");
        assertEquals(c1, p1.getCoordinate());
        assertEquals('H', p1.getOrientation());
        Coordinate c2 = new Coordinate("D5");
        Placement p2 = new Placement("D5H");
        assertEquals(c2, p2.getCoordinate());
        assertEquals('H', p2.getOrientation());
        Coordinate c3 = new Coordinate("A9");
        Placement p3 = new Placement("A9v");
        assertEquals(c3, p3.getCoordinate());
        assertEquals('V', p3.getOrientation());
        Coordinate c4 = new Coordinate("Z0");
        Placement p4 = new Placement("Z0h");
        assertEquals(c4, p4.getCoordinate());
        assertEquals('H', p4.getOrientation());
    }
    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("00H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAH"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("[0H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A/H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A:H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AH"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A12"));
    }

}