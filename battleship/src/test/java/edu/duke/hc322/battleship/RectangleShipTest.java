package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class RectangleShipTest {

    @Test
    void test_makeCoords() {
        HashSet<Coordinate> expectedSet = new HashSet<>();
        Coordinate upperLeft = new Coordinate(1,2);
        expectedSet.add(upperLeft);
        expectedSet.add(new Coordinate(2,2));
        expectedSet.add(new Coordinate(3,2));

        HashSet<Coordinate> actual = RectangleShip.makeCoords(upperLeft, 1,3);
        assertTrue(expectedSet.containsAll(actual));
    }

    @Test
    void test_rectangle_ship(){
        Coordinate upperLeft = new Coordinate(1,2);
        RectangleShip rs = new RectangleShip("submarine", upperLeft, 2,3,'s', '*');
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(2,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(3,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,3)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(2,3)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(3,3)));
        assertEquals(rs.getName(), "submarine");
    }

    @Test
    void test_record_hit_at(){
        RectangleShip<Character> s = new RectangleShip<Character>("submarine", new Coordinate(0,0),1,3 , 's', '*');
        HashMap<Coordinate, Boolean> myPieces = new HashMap<>();
        myPieces.put(new Coordinate(0,0), false);
        myPieces.put(new Coordinate(1,0), true);
        myPieces.put(new Coordinate(2,0), true);
        s.recordHitAt(new Coordinate(1,0));
        s.recordHitAt(new Coordinate(2,0));
        assertEquals(s.occupiesCoordinates(new Coordinate(1,0)), myPieces.get(new Coordinate(1,0)));
        assertEquals(s.occupiesCoordinates(new Coordinate(2,0)), myPieces.get(new Coordinate(1,0)));
        assertThrows(IllegalArgumentException.class, () -> s.recordHitAt(new Coordinate(4,0)));
    }

    @Test
    void test_was_hit_at(){
        RectangleShip<Character> s = new RectangleShip<Character>("submarine", new Coordinate(0,0),1,3 , 's', '*');
        s.recordHitAt(new Coordinate(1,0));
        s.recordHitAt(new Coordinate(2,0));
        assertEquals(s.occupiesCoordinates(new Coordinate(1,0)), s.wasHitAt(new Coordinate(1,0)));
        assertEquals(s.occupiesCoordinates(new Coordinate(2,0)), s.wasHitAt(new Coordinate(2,0)));
        assertThrows(IllegalArgumentException.class, () -> s.wasHitAt(new Coordinate(4,0)));
    }

    @Test
    void test_is_sunk(){
        RectangleShip<Character> s = new RectangleShip<Character>("submarine", new Coordinate(0,0),1,3 , 's', '*');
        s.recordHitAt(new Coordinate(0,0));
        s.recordHitAt(new Coordinate(1,0));
        s.recordHitAt(new Coordinate(2,0));
        assertTrue(s.isSunk());
    }

    @Test
    void test_get_display_info_at(){
        RectangleShip<Character> s = new RectangleShip<Character>("submarine", new Coordinate(0,0),1,3 , 's', '*');
        s.recordHitAt(new Coordinate(0,0));
        assertEquals(s.getDisplayInfoAt(new Coordinate(0,0), true), '*');
        assertEquals(s.getDisplayInfoAt(new Coordinate(1,0), true), 's');
        assertThrows(IllegalArgumentException.class, () -> s.getDisplayInfoAt(new Coordinate(4,0), true));

    }

    @Test
    void test_get_coordinates(){
        RectangleShip<Character> s = new RectangleShip<Character>("submarine", new Coordinate(0,0),1,3 , 's', '*');
        Iterable<Coordinate> actual = s.getCoordinates();
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(0,0));
        expected.add(new Coordinate(1,0));
        expected.add(new Coordinate(2,0));
        assertEquals(actual.toString(), expected.toString());
    }

    @Test
    void test_to_string(){
        RectangleShip<Character> s = new RectangleShip<Character>("submarine",
                new Coordinate(0,0),1,3 , 's', '*');
        assertEquals(s.toString(), "submarine contains: C0, B0, A0, \n");
    }

    @Test
    void test_move(){
        RectangleShip<Character> s = new RectangleShip<Character>("submarine",
                new Coordinate(0,0),1,3 , 's', '*');
        s.move(3,0);
        assertEquals(s.getDisplayInfoAt(new Coordinate(2,3), true), 's');
        assertEquals(s.getDisplayInfoAt(new Coordinate(1,3), true), 's');
        assertEquals(s.getDisplayInfoAt(new Coordinate(0,3), true), 's');

    }
}