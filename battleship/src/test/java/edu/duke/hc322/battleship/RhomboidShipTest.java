package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RhomboidShipTest {
    @Test
    void test_ship_up(){
        Coordinate upperLeft = new Coordinate(0,0);
        RhomboidShip rs = new RhomboidShip("Carrier", upperLeft, 2,5, 'U','s', '*');
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,1)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(1,1)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(4,0)));

        assertTrue(rs.occupiesCoordinates(new Coordinate(0,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(2,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(3,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(2,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(3,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(4,1)));
    }

    @Test
    void test_ship_right(){
        Coordinate upperLeft = new Coordinate(0,0);
        RhomboidShip rs = new RhomboidShip("Carrier", upperLeft, 5,2, 'R','s', '*');
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,0)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(1,3)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(1,4)));

        assertTrue(rs.occupiesCoordinates(new Coordinate(0,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,3)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,4)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,2)));
    }

    @Test
    void test_ship_down(){
        Coordinate upperLeft = new Coordinate(0,0);
        RhomboidShip rs = new RhomboidShip("Carrier", upperLeft, 2,5, 'D','s', '*');
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,1)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(3,0)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(4,0)));

        assertTrue(rs.occupiesCoordinates(new Coordinate(0,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(2,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(2,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(3,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(4,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,1)));
    }



    @Test
    void test_ship_LEFT(){
        Coordinate upperLeft = new Coordinate(0,0);
        RhomboidShip rs = new RhomboidShip("Carrier", upperLeft, 5,2, 'L','s', '*');
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,0)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,1)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(1,4)));

        assertTrue(rs.occupiesCoordinates(new Coordinate(0,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,3)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,4)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,3)));
    }
}