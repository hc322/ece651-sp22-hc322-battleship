package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleShipDisplayInfoTest {

    @Test
    void test_getInfo() {
        SimpleShipDisplayInfo<Character> info = new SimpleShipDisplayInfo<>('s', '*');
        char hit = info.getInfo(new Coordinate("A0"), true);
        assertEquals('*', hit);

    }
}