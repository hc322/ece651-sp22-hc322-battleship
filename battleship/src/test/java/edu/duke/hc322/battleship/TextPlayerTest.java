package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class TextPlayerTest {

    @Test
    void test_read_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
        String prompt = "Please enter a location for a ship:\n";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back
            assertEquals(prompt, bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }

    @Test
    void test_do_one_placement() throws IOException{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 4, "A0V\n", bytes);

        Board<Character> b = new BattleShipBoard<Character>(4, 4,'X');
        Placement p = new Placement("A0V");
        RectangleShip<Character> ship = new RectangleShip<Character>("Destroyer", p.getCoordinate(), 1, 2, 's', '*');
        String emptyBoard = new BoardTextView(b).displayMyOwnBoard();
        b.tryAddShip(ship);
        BoardTextView view = new BoardTextView(b);
        String prompt ="Where do you want to place a Submarine?\n";
        String instruction = "Player A: you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2 \n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n";
        player.placementIntro();
        assertEquals(emptyBoard +  instruction, bytes.toString());
        bytes.reset();
        player.doOnePlacement("Submarine", (a)->player.shipFactory.makeSubmarine(p));
        assertEquals("Player "+ player.name + " " + prompt+ view.displayMyOwnBoard(), bytes.toString());
        bytes.reset();
    }

    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory, 3, 3);
    }
}