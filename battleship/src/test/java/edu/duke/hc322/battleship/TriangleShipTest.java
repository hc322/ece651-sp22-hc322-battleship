package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleShipTest {
    @Test
    void test_triangle_ship_up(){
        Coordinate upperLeft = new Coordinate(0,0);
        TriangleShip rs = new TriangleShip("Battleship", upperLeft, 3,2, 'U','s', '*');
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,1)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,2)));
    }

    @Test
    void test_triangle_ship_right(){
        Coordinate upperLeft = new Coordinate(0,0);
        TriangleShip rs = new TriangleShip("battleship", upperLeft, 2,3, 'R','s', '*');
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,1)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(2,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(2,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,1)));
    }

    @Test
    void test_triangle_ship_down(){
        Coordinate upperLeft = new Coordinate(0,0);
        TriangleShip rs = new TriangleShip("battleship", upperLeft, 3,2, 'D','s', '*');
        assertFalse(rs.occupiesCoordinates(new Coordinate(1,0)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(1,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,2)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,1)));
    }



    @Test
    void test_triangle_ship_left(){
        Coordinate upperLeft = new Coordinate(0,0);
        TriangleShip rs = new TriangleShip("battleship", upperLeft, 2,3, 'L','s', '*');
        assertFalse(rs.occupiesCoordinates(new Coordinate(0,0)));
        assertFalse(rs.occupiesCoordinates(new Coordinate(2,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(0,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,0)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(1,1)));
        assertTrue(rs.occupiesCoordinates(new Coordinate(2,1)));
    }

}