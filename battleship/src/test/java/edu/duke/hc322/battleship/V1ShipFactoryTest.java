package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class V1ShipFactoryTest {
    @Test
    void test_make_ship(){
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> dst_1 = f.makeDestroyer(v1_1);
        checkShip(dst_1, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
    }


    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate ... expectedLocs){
        assertEquals(testShip.getName(), expectedName);
        for(int i = 0; i < expectedLocs.length; i++){
            assertEquals(testShip.getDisplayInfoAt(expectedLocs[i], true), expectedLetter);
        }
    }
}