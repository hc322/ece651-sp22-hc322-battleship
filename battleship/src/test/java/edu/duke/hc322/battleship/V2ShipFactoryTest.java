package edu.duke.hc322.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class V2ShipFactoryTest {
    @Test
    void test_make_ship_destroyer(){
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> dst_1 = f.makeDestroyer(v1_1);
        checkShip(dst_1, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
    }
    @Test
    void test_make_ship_submarine(){
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(0, 0), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);
        checkShip(dst, "Submarine", 's', new Coordinate(0, 0), new Coordinate(1, 0));
        Placement v1_1 = new Placement(new Coordinate(0, 0), 'H');
        Ship<Character> dst_1 = f.makeSubmarine(v1_1);
        checkShip(dst_1, "Submarine", 's', new Coordinate(0, 0), new Coordinate(0, 1));
    }

    @Test
    void test_make_ship_battleShip(){
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(0, 0), 'U');
        Ship<Character> dst = f.makeBattleship(v1_2);
        checkShip(dst, "Battleship", 'b', new Coordinate(0, 1), new Coordinate(1, 0), new Coordinate(1,1), new Coordinate(1,2));
        Placement v1_1 = new Placement(new Coordinate(0, 0), 'D');
        Ship<Character> dst_1 = f.makeBattleship(v1_1);
        checkShip(dst_1, "Battleship", 'b', new Coordinate(0, 0), new Coordinate(0, 1), new Coordinate(0,2), new Coordinate(1,1));
        Placement v1_3 = new Placement(new Coordinate(0, 0), 'L');
        Ship<Character> dst_3 = f.makeBattleship(v1_3);
        checkShip(dst_3, "Battleship", 'b', new Coordinate(0, 1), new Coordinate(1, 0), new Coordinate(1,1), new Coordinate(2,1));
        Placement v1_4 = new Placement(new Coordinate(0, 0), 'R');
        Ship<Character> dst_4 = f.makeBattleship(v1_4);
        checkShip(dst_4, "Battleship", 'b', new Coordinate(0, 0), new Coordinate(1, 0), new Coordinate(2,0), new Coordinate(1,1));
    }
    @Test
    void test_make_ship_carrier(){
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(0, 0), 'U');
        Ship<Character> dst = f.makeCarrier(v1_2);
        checkShip(dst, "Carrier", 'c', new Coordinate(0,0),
        new Coordinate(1,0),
        new Coordinate(2,0),
        new Coordinate(3,0),
        new Coordinate(2,1),
        new Coordinate(3,1),
        new Coordinate(4,1));
        Placement v1_1 = new Placement(new Coordinate(0, 0), 'D');
        Ship<Character> dst_1 = f.makeCarrier(v1_1);
        checkShip(dst_1, "Carrier", 'c', new Coordinate(0,0),
        new Coordinate(1,0),
        new Coordinate(2,0),
        new Coordinate(2,1),
        new Coordinate(3,1),
        new Coordinate(4,1),
        new Coordinate(1,1));
        Placement v1_3 = new Placement(new Coordinate(0, 0), 'L');
        Ship<Character> dst_3 = f.makeCarrier(v1_3);
        checkShip(dst_3, "Carrier", 'c', new Coordinate(0,2),
        new Coordinate(0,3),
        new Coordinate(0,4),
        new Coordinate(1,0),
        new Coordinate(1,1),
        new Coordinate(1,2),
        new Coordinate(1,3));
        Placement v1_4 = new Placement(new Coordinate(0, 0), 'R');
        Ship<Character> dst_4 = f.makeCarrier(v1_4);
        checkShip(dst_4, "Carrier", 'c', new Coordinate(0,1),
        new Coordinate(0,2),
        new Coordinate(0,3),
        new Coordinate(0,4),
        new Coordinate(1,0),
        new Coordinate(1,1),
        new Coordinate(1,2));
    }



    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate ... expectedLocs){
        //assertEquals(testShip.getName(), expectedName);
        for(int i = 0; i < expectedLocs.length; i++){
            assertEquals(testShip.getDisplayInfoAt(expectedLocs[i], true), expectedLetter);
        }
    }
}